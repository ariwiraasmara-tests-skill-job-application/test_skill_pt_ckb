package com.ariwiraasmara.testskill;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Login extends AppCompatActivity {

    Context ctx;
    EditText txt_user, txt_pass;
    Button btn_login;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        fieldVariable();
        login();
    }

    protected void fieldVariable() {
        ctx       = Login.this;
        txt_user  = (EditText) findViewById(R.id.txt_userid);
        txt_pass  = (EditText) findViewById(R.id.txt_pass);
        btn_login = (Button) findViewById(R.id.btn_login);
        textView  = (TextView) findViewById(R.id.txt_msg);
    }

    protected void login() {
        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(getApplicationContext(), "Hai!", Toast.LENGTH_LONG).show();

                String url = "192.168.43.18/testskill_barumi/public/api/login/";
                RequestQueue queue = Volley.newRequestQueue(ctx);
                StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                        new Response.Listener<String>()
                        {
                            @Override
                            public void onResponse(String response) {
                                // response
                                txt_user.setText("");
                                txt_pass.setText("");
                                textView.setText(response);
                                Log.d("API Response", response);
                            }
                        },
                        new Response.ErrorListener()
                        {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // error
                                Log.d("API Error Response", error.toString());
                            }
                        }
                ) {
                    @Override
                    protected Map<String, String> getParams()
                    {
                        Map<String, String>  params = new HashMap<String, String>();
                        params.put("id", txt_user.getText().toString());
                        params.put("pass", txt_pass.getText().toString());

                        return params;
                    }
                };
                queue.add(postRequest);
                System.out.println("Success!");
                textView.setText("Success!");
            }
        });
    }

}