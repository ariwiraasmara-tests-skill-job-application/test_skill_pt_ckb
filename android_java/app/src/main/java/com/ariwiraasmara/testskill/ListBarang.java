package com.ariwiraasmara.testskill;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

public class ListBarang extends AppCompatActivity {

    Context ctx;
    Boolean islogin;
    String userid;
    ListView list;
    Intent intent;
    Button btn_add;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_barang);
        fieldVariable();
        isLogin();
        listBarang();
        buttonAction();
    }

    protected void fieldVariable() {
        ctx     = ListBarang.this;
        intent  = getIntent();
        list    = (ListView) findViewById(R.id.listBarang);
        btn_add = (Button) findViewById(R.id.btn_add);

    }

    protected void isLogin() {
        //userid  = intent.getStringExtra("userid");
        //islogin = intent.getBooleanExtra("islogin", false);
        userid  = "user1";
        islogin = true;
        if( (userid.equals("") || userid.equals(null)) && islogin.equals(false) ) {
            intent = new Intent(this, Login.class);
            startActivity(intent);
        }
    }

    protected void listBarang() {
        RequestQueue queue = Volley.newRequestQueue(ctx);
        final String url = "192.168.43.18/testskill_barumi/public/api/barang/";

        // prepare the Request
        StringRequest getRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // display response
                        btn_add.setText(response.toString());
                        Log.d("API Response", response.toString());
                    }
                },
                new Response.ErrorListener()
                {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("API Error Response", error.toString());
                    }
                }
        );

        // add it to the RequestQueue
        queue.add(getRequest);
    }

    protected void buttonAction() {
        btn_add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                intent = new Intent(ctx, NE_Barang.class);
                intent.putExtra("isnewedit","new");
                startActivity(intent);
            }
        });
    }

}