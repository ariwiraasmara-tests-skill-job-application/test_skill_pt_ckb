package com.ariwiraasmara.testskill;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.*;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

public class NE_Barang extends AppCompatActivity {

    Context ctx;
    String newedit;
    Intent intent;
    EditText txt_id, txt_nama, txt_minstok, txt_stokava;
    Button btn_save;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ne_barang);
        fieldVariable();
        isNewEdit();
        getData();
    }

    protected void fieldVariable() {
        ctx     = NE_Barang.this;
        intent  = getIntent();
        newedit = intent.getStringExtra("isnewedit");
        txt_id  = (EditText) findViewById(R.id.ne_txt_id);
        txt_nama  = (EditText) findViewById(R.id.ne_txt_nama);
        txt_minstok  = (EditText) findViewById(R.id.ne_txt_minstok);
        txt_stokava  = (EditText) findViewById(R.id.ne_txt_stokava);
        btn_save = (Button) findViewById(R.id.btn_save);
    }

    protected void isNewEdit() {
        if(newedit.equals("new")) {
            btn_save.setText("Save");
        }
        else if(newedit.equals("edit")) {
            btn_save.setText("Edit");
        }
        else {
            btn_save.setText("NULL");
            btn_save.setEnabled(false);
        }
    }

    protected void getData() {
        if(newedit.equals("edit")) {
            RequestQueue queue = Volley.newRequestQueue(ctx);
            final String url = "192.168.43.18/testskill_barumi/public/api/barang/" + intent.getStringExtra("idb");

            // prepare the Request
            JsonObjectRequest getRequest = new JsonObjectRequest(Request.Method.GET, url, null,
                    new Response.Listener<JSONObject>()
                    {
                        @Override
                        public void onResponse(JSONObject response) {
                            // display response
                            Log.d("Response", response.toString());
                        }
                    },
                    new Response.ErrorListener()
                    {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.d("Error.Response", error.toString());
                        }
                    }
            );
        }
    }

    protected void buttonAction() {

    }

    protected void save() {

    }

    protected void update() {

    }

}