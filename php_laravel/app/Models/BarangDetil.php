<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BarangDetil extends Model
{
    use HasFactory;
    protected $guarded = [];
    protected $table = 'barang_detil';
    protected $fillable = ["kode_barang",
                            "userid",
                            "nama_barang",
                            "min_stok",
                            "stok_tersedia",];

    public $timestamps = false;
    const CREATED_AT = 'creation_date';
    const UPDATED_AT = 'updated_date';

}
