<?php

namespace App\Http\Controllers;

use App\Models\User2;
use App\Http\Requests\StoreUser2Request;
use App\Http\Requests\UpdateUser2Request;

class User2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request){
        //
        $where = array("userid" => $request->id, "password"=>$request->pass);
        $user = User::where($where)->first();
        if(!$user) {
            return response()->json(['msg' => 'Login Fail!',
                                    'layer' => 1,
                                    'success' => 0,
                                    'place'=>'UserController@login'], 500);
        }
        $data = User::where($where)->get();
        return response()->json(['msg' => 'Login Success', 'success' => 1, 'data'=>$data], 201);
    }

    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreUser2Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreUser2Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User2  $user2
     * @return \Illuminate\Http\Response
     */
    public function show(User2 $user2)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User2  $user2
     * @return \Illuminate\Http\Response
     */
    public function edit(User2 $user2)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateUser2Request  $request
     * @param  \App\Models\User2  $user2
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateUser2Request $request, User2 $user2)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User2  $user2
     * @return \Illuminate\Http\Response
     */
    public function destroy(User2 $user2)
    {
        //
    }
}
