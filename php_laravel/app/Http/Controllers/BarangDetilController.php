<?php

namespace App\Http\Controllers;

use App\Models\BarangDetil;
use Illuminate\Http\Request;
use App\Http\Requests\StoreBarangDetilRequest;
use App\Http\Requests\UpdateBarangDetilRequest;

class BarangDetilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
        $data = BarangDetil::get();
        return response()->json(['msg' => 'All Barang', 'success' => 1, 'data'=>$data], 201);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        //
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreBarangDetilRequest  $request
     * @return \Illuminate\Http\Response
     */
    //public function store(StoreBarangDetilRequest $request) {
    public function store(Request $request) {
        //
        $res = BarangDetil::create([
            'kode_barang'   => $request->kode,
            'userid'        => $request->userid,
            'nama_barang'   => $request->nama,
            'min_stok'      => $request->minstok,
            'stok_tersedia' => $request->stokava,
        ]);
        if(!$res) return response()->json(['msg' => 'Fail Store Barang!', 'success' => 0], 500);
        return response()->json(['msg' => 'Success Store Barang!', 'success' => 1], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\BarangDetil  $barangDetil
     * @return \Illuminate\Http\Response
     */
    public function show($id) { //(BarangDetil $barangDetil) {
        //
        $data = BarangDetil::where("kode_barang", '=', $id)->get();
        return response()->json(['msg' => 'Detil Barang', 'success' => 1, 'data'=>$data], 201);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\BarangDetil  $barangDetil
     * @return \Illuminate\Http\Response
     */
    public function edit(BarangDetil $barangDetil) {
        //
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateBarangDetilRequest  $request
     * @param  \App\Models\BarangDetil  $barangDetil
     * @return \Illuminate\Http\Response
     */
    //public function update(UpdateBarangDetilRequest $request, BarangDetil $barangDetil) {
    public function update(Request $request, $id) {
        //
        $res = BarangDetil::where('kode_barang', '=', $id)->update([
            'nama_barang'   => $request->nama,
            'min_stok'      => $request->minstok,
            'stok_tersedia' => $request->stokava,
        ]);
        if(!$res) return response()->json(['msg' => 'Fail Update Barang!', 'success' => 0], 500);
        return response()->json(['msg' => 'Success Update Barang!', 'success' => 1], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\BarangDetil  $barangDetil
     * @return \Illuminate\Http\Response
     */
    public function destroy(BarangDetil $barangDetil)
    {
        //
    }
}
