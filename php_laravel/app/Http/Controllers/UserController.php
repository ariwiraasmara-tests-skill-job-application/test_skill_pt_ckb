<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserController extends Controller {
    //
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\asmcp_1001_userid  $asmcp_1001_userid
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request){
        //
        $where = array("userid" => $request->id, "password"=>$request->pass);
        $user = User::where($where)->first();
        if(!$user) {
            return response()->json(['msg' => 'Login Fail!',
                                    'layer' => 1,
                                    'success' => 0,
                                    'place'=>'UserController@login'], 500);
        }
        $data = User::where($where)->get();
        return response()->json(['msg' => 'Login Success', 'success' => 1, 'data'=>$data], 201);
    }
}
