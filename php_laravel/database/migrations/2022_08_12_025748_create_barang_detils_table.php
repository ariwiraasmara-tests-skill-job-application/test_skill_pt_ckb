<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('barang_detil', function (Blueprint $table) {
            $table->string('kode_barang');
            $table->string('userid');
            $table->string('nama_barang');
            $table->integer('min_stok');
            $table->integer('stok_tersedia');
            //$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('barang_detil');
    }
};
