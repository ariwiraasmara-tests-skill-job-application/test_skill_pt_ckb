<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('/login', '\App\Http\Controllers\UserController@login');
Route::get('/user', '\App\Http\Controllers\UserController@getuser');

Route::get('/barang', '\App\Http\Controllers\BarangDetilController@index');
Route::get('/barang/{id}', '\App\Http\Controllers\BarangDetilController@show');
Route::post('/barang/insert', '\App\Http\Controllers\BarangDetilController@store');
Route::put('/barang/update/{id}', '\App\Http\Controllers\BarangDetilController@update');
